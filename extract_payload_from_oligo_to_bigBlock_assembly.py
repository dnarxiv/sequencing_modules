#!/usr/bin/python3

import os
import sys
import inspect
import argparse


currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, os.path.dirname(currentdir)+"/synthesis_modules")
import dna_file_reader as dfr


OVERHANG_SIZE = 4 # size of the overlapping overhangs used to link the fragments together

PRIMER_LENGTH = 20 # length of primers used in the PCR

total_bsaI_site_length = 11 # length removed from first and last oligos of each block by the bsaI site digestion

OLIGO_LEN = 56 # length of oligos without the overhang

OLIGO_PER_BLOCK = 1# 10 # number of oligo in each block
BLOCK_PER_BIGBLOCK = 10 # number of block in each bigBlock

total_block_len = 472 # value used if a block has 1 synthesized oligo, # contains also the primer length if first of last block in a bigBlock

if OLIGO_PER_BLOCK >= 2: # case blocks are assembly of multiple oligos, the extremums are shorter because bsaI digestion
    total_block_len = OLIGO_LEN * OLIGO_PER_BLOCK + (OLIGO_PER_BLOCK-1) * OVERHANG_SIZE - 2*total_bsaI_site_length # N oligos, N-1 overhangs 

total_bigBlock_len = total_block_len * BLOCK_PER_BIGBLOCK + (BLOCK_PER_BIGBLOCK-1) * OVERHANG_SIZE # M blocks, M-1 overhangs, 2*M primers


class Full_assembly:
    
    def __init__(self, sequence):
        
        self.sequence = sequence
        self.bigBlock_number = self.get_bigBlock_number()
        self.bigBlock_list, self.overhangs_assembly_list = self.fragmentation_assembly()
        self.payload = self.extract_payload_assembly()
        
    
    def get_bigBlock_number(self) -> int:
        """
        verify if the full assembly has an expected length, returns the number of bigBlocks
        """
        
        bigBlock_number, rest =  divmod(len(self.sequence), total_bigBlock_len)
        
        if rest != (bigBlock_number-1) * OVERHANG_SIZE: # there should be {bigBlock_number-1} time overhangs between the bigBlocks
            print("error payload extraction : incoherent length,",str(bigBlock_number),"bigBlocks with a rest of",str(rest),"bases")
            exit(1)
            
        return bigBlock_number


    def fragmentation_assembly(self) -> (list, list):
        """
        fragment the assembly to get the list of bigBlocks and the overhangs used to link them
        """
        
        bigBlock_list, overhangs_list = [], []
        
        index = 0
        for i in range(self.bigBlock_number-1):
            bigBlock_sequence = self.sequence[index: index+total_bigBlock_len]
            bigBlock_list.append( BigBlock(bigBlock_sequence) )
            
            overhang_sequence = self.sequence[index+total_bigBlock_len: index+total_bigBlock_len+OVERHANG_SIZE]
            overhangs_list.append(overhang_sequence)
            
            index = index + total_bigBlock_len + OVERHANG_SIZE
            
        bigBlock_sequence = self.sequence[index:]
        bigBlock_list.append( BigBlock(bigBlock_sequence) )
        print(overhangs_list)
        return bigBlock_list, overhangs_list
    
    
    def extract_payload_assembly(self) -> str:
        """
        get all the payload in the assembly
        """
        
        payload = ""
        for bigBlock in self.bigBlock_list:
            payload += bigBlock.payload
        return payload
    
            

class BigBlock:
    
    def __init__(self, sequence):
        
        self.sequence = sequence
        self.block_list, self.overhangs_bigBlock_list = self.fragmentation_bigBlock()
        self.payload = self.extract_payload_bigBlock()
    
    
    def fragmentation_bigBlock(self) -> (list, list):
        """
        fragment the bigBlock to get the list of blocks and the overhangs used to link them
        each bigBlock contains primers at the start and the end
        """
        block_list, overhangs_list = [], []
        
        # ignore the primer at the start        
        block_sequence = self.sequence[PRIMER_LENGTH:total_block_len] # the total block len conteains the primer length
        block_list.append( Block(block_sequence) )
        
        overhang_sequence = self.sequence[total_block_len: total_block_len+OVERHANG_SIZE]
        overhangs_list.append(overhang_sequence)
            
        index = total_block_len + OVERHANG_SIZE
        
        for i in range(1, BLOCK_PER_BIGBLOCK-1):
            block_sequence = self.sequence[index: index+total_block_len]
            block_list.append( Block(block_sequence) )
            
            overhang_sequence = self.sequence[index+total_block_len: index+total_block_len+OVERHANG_SIZE]
            overhangs_list.append(overhang_sequence)
            
            index = index + total_block_len + OVERHANG_SIZE
        
        block_sequence = self.sequence[index: -PRIMER_LENGTH] # ignore the primer at the end
        block_list.append( Block(block_sequence) )
        print(overhangs_list)
        return block_list, overhangs_list


    def extract_payload_bigBlock(self) -> str:
        
        payload = ""
        for block in self.block_list:
            payload += block.payload
        return payload
    


class Block:
    
    def __init__(self, sequence):
        
        self.sequence = sequence
        self.oligo_list, self.overhangs_oligo_list  = self.fragmentation_block()
        self.payload = self.extract_payload_block()
        
    
    def fragmentation_block(self) -> (list, list):
        """
        fragment the block to get the list of oligos and the overhangs used to link them
        """
        
        if OLIGO_PER_BLOCK == 1: # case where block are directly fully synthesized
            return [Oligo(self.sequence)], []
        
        oligo_list, overhangs_list = [], []
        
        first_oligo_size = OLIGO_LEN-total_bsaI_site_length
        oligo_list = [ Oligo(self.sequence[PRIMER_LENGTH:PRIMER_LENGTH+first_oligo_size]) ] # add first oligo of a block, shorter because of bsaI digestion
        
        overhang_sequence = self.sequence[first_oligo_size: first_oligo_size+OVERHANG_SIZE]
        overhangs_list.append(overhang_sequence)
            
        index = first_oligo_size + OVERHANG_SIZE
        
        for i in range(1, OLIGO_PER_BLOCK-1):
            oligo_sequence = self.sequence[index: index+OLIGO_LEN]
            oligo_list.append(Oligo(oligo_sequence))
            
            overhang_sequence = self.sequence[index+OLIGO_LEN: index+OLIGO_LEN+OVERHANG_SIZE]
            overhangs_list.append(overhang_sequence)
            
            index = index + OLIGO_LEN + OVERHANG_SIZE
            
        last_oligo_size = OLIGO_LEN-total_bsaI_site_length
        oligo_list.append( Oligo(self.sequence[-last_oligo_size-PRIMER_LENGTH:-PRIMER_LENGTH])) # add last oligo of a block, bsaI digestion & no overhang
                    
        return oligo_list, overhangs_list


    def extract_payload_block(self) -> str:
        
        payload = ""
        for oligo in self.oligo_list:
            payload += oligo.payload
        
        return payload


class Oligo:
    
    def __init__(self, sequence):
        self.payload = sequence


def count_anomaly(consensus_seq: str) -> None:
    
    anomaly_count = 0
    z_index = 0
    for i, base in enumerate(consensus_seq):
        if z_index == 1 or z_index == 4:
            prev_base = consensus_seq[i-1]
            if base in ['A','T'] and prev_base in ['A','T'] or base in ['C','G'] and prev_base in ['C','G']:
                anomaly_count += 1
        z_index = (z_index+1) % 5
    print(anomaly_count)


def extract_payload(input_path: str, output_path: str) -> None:
    """
    remove overhangs and primers from the consensus sequence and save the resulting payload
    returns an error if the size is incoherent
    """
    _, consensus_seq = dfr.read_single_sequence_fasta(input_path)
    
    assembly = Full_assembly(consensus_seq)
    result_sequence = assembly.payload
    dfr.save_sequence_to_fasta("payload", result_sequence, output_path)
    


def concatenate_eBlocks(input_path: str, output_path: str):
    
    block_dict = dfr.read_fasta(input_path)
    block_list = list(block_dict.values())

    # create a file containing the concatenation of all blocks with overhangs
    total_sequence = ""
    for i, block in enumerate(block_list[:-1]):
        if (i+1)%10 == 0:
            total_sequence += block[OVERHANG_SIZE:-OVERHANG_SIZE] # remove Moverhang
        else:
            total_sequence += block[OVERHANG_SIZE:]
    total_sequence += block_list[-1][OVERHANG_SIZE:-OVERHANG_SIZE]
    dfr.save_sequence_to_fasta("total_sequence", total_sequence, output_path)


if __name__ == "__main__":
    
    parser = argparse.ArgumentParser(description='extract the payload from an ordered eBlocks assembly')
    parser.add_argument('-i', action='store', dest='input_path', required=True,
                        help='fasta file to the consensus sequence')
    parser.add_argument('-o', action='store', dest='output_path', required=True,
                        help='fasta file to save the result')

    
    arg = parser.parse_args()
    
    print("payload extraction...")

    extract_payload(arg.input_path, arg.output_path)
                
    print("\tcompleted !")

