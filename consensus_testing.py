#!/usr/bin/python3

import os
import sys
import inspect
import time
import random


import reads_consensus_class as rcc

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, os.path.dirname(currentdir)+"/synthesis_modules")
import dna_file_reader as dfr
import synthesis_simulation as ss


start_primer, stop_primer = "TCTCGGGCAACGTTTCACTT", "GAATTGGATAGTGGGAGCCT"


def compute_checksum(payload_sequence: str) -> str:
    """
    calculate a check_sum associated with the sequence
    XOR operation of all 16 bits parts of the sequence converted in binary
    """
    CHECK_SUM_SIZE = 16

    binary_sequence = payload_sequence.replace("A", "00").replace("C", "11").replace("G", "01").replace("T", "10")

    # split the string into substrings of CHECK_SUM_SIZE
    splitted_binary_string = [binary_sequence[i:i+CHECK_SUM_SIZE] for i in range(0, len(binary_sequence), CHECK_SUM_SIZE)]
    rest = len(binary_sequence) % CHECK_SUM_SIZE
    if rest > 0:
        splitted_binary_string += [ binary_sequence[-rest:] + "0"*(CHECK_SUM_SIZE-rest)]

    bin_sum = "0"
    for binary_split in splitted_binary_string:

        int_sum = int(bin_sum, 2) ^ int(binary_split, 2) # XOR sum
        bin_sum = str(bin(int_sum))[2:] # convert back into binary string

    bin_checksum = bin_sum.zfill(CHECK_SUM_SIZE) # fill the beginning with 0 to get the correct size

    dna_checksum = ""
    # convert back in dna
    for i in range(8):
        bit2 = bin_checksum[2*i:2*i+2]
        dna_checksum += bit2.replace("00", "A").replace("11", "C").replace("01", "G").replace("10", "T")

    return dna_checksum


def generate_references_sequences(references_seq_dir) -> dict:
    """
    create random sequences with a checksum to use as references for the tests
    """

    h_max = 3 # maxi homopolymere size

    # generate references sequences for all sizes
    for seq_size in [5000]: #[500,1000,3000,

        references_seq_path = references_seq_dir+"/ref_size_"+str(seq_size)+".fasta"

        generated_seq_dict = {}

        for seq_num in range(100000):
            sequence = ""
            # generate the sequence until reaching the size minus len of the 2 primers and len of checksum
            while len(sequence) < seq_size - 2*20 - 8:
                alphabet = ["A", "G", "C", "T"]
                letter = random.choice(alphabet)
                if sequence[-h_max:] == letter*h_max:  # if the end of the sequence is a homopolymer of this letter
                    alphabet.remove(letter)
                    letter = random.choice(alphabet)  # then pick another one
                sequence += letter

            # compute and add a checksum
            checksum = compute_checksum(sequence)
            sequence += checksum

            # add the primers
            sequence = start_primer + sequence + dfr.reverse_complement(stop_primer)
            generated_seq_dict["sequence_"+str(seq_num)] = sequence

        dfr.save_dict_to_fasta(generated_seq_dict, references_seq_path)



def generate_random_reads(references_file_path, ref_number, coverage, result_file):
    """
    generate a read file containing the simulated reads from {assembly_number} sequences, {reads_per_file} times each
    """

    i_error, d_error, s_error = 0.005, 0.01, 0.005 # ~2% error rate

    ref_dict = dfr.read_fasta(references_file_path)
    selected_refs = dict(list(ref_dict.items())[:ref_number]) # select the right amount

    output_read_dict = {}

    for seq_name, sequence in selected_refs.items():

        for i in range(coverage):
            simulated_read_seq = ss.add_errors(sequence, i_error, d_error, s_error)
            output_read_dict[seq_name+"_"+str(i)] = simulated_read_seq

    dfr.save_dict_to_fastq(output_read_dict, result_file)


def verify_consensus(consensus_path, references_file_path, ref_number):
    """
    return the number of sequences with correct checksum that are in the referrences
    and the number of false positive that has a correct checksum
    """
    consensus_dict = dfr.read_fasta(consensus_path)

    # filter the consensus sequences with the checksum
    filtered_consensus_dict = {}
    for name, sequence in consensus_dict.items():
        payload = sequence[20:-28]
        checksum = sequence[-28:-20]
        # test if the checksum corresponds to the payload
        if compute_checksum(payload) == checksum:
            filtered_consensus_dict[name] = sequence

    # get the reference sequences
    ref_dict = dfr.read_fasta(references_file_path)
    selected_refs = dict(list(ref_dict.items())[:ref_number])

    ref_seq_dict = {v: 1 for k, v in selected_refs.items()}

    score = 0

    # test the presence of the consensus in the references sequences
    for seq_name, sequence in filtered_consensus_dict.items():
        score += ref_seq_dict.get(sequence, 0)

    return score, len(filtered_consensus_dict)-score


def count_shared_sequences(reads_number, kmer_size, occ_size):
    """
    function for the 28m sequenced reads
    filter the sequences with the checksum and compare to the references
    """

    print("count",reads_number,kmer_size,occ_size)

    consensus_dict = dfr.read_fasta("test_consensus/images_sequences/results/"+str(reads_number)+"_k"+str(kmer_size)+"_occ2.fasta")

    payload_id_check_consensus_dict = {} # keep the sequences that have a correct checksum

    # extract and test checksum
    for name, seq in list(consensus_dict.items()):
        occurrences = int(name.split("score=")[1])
        # filter by number of occurrences
        if occurrences < occ_size:
            continue

        id_seq = seq[96:101]
        checksum_seq = seq[101:105]
        payload_seq = seq[27:96] + seq[105:178]


        # filter if correct id
        if not id_seq in ["TTACA", "AATAT", "ATGAG", "AACCG"]:
            continue


        # filter by correct checksum
        accepted = True
        for i, base in enumerate(['A','C','T','G']):
            base_count = payload_seq.count(base)
            if checksum_seq[i] in ['A', 'T'] and base_count % 2:
                accepted = False
                break
            elif checksum_seq[i] in ['C', 'G'] and not base_count % 2:
                accepted = False
                break

        if accepted:
            payload_id_check = seq[27:178]
            if not payload_id_check in payload_id_check_consensus_dict:
                # avoid duplicate payload+id+checksum
                payload_id_check_consensus_dict[payload_id_check] = name
            # else do not keep the sequence since the checksum is invalid

    #print(len(consensus_dict)-len(payload_id_check_consensus_dict),"sequences rejected;",len(consensus_dict),"->",len(payload_id_check_consensus_dict))

    reference_dict = dfr.read_fasta("test_consensus/images_sequences/references.fasta")
    payload_id_check_reference_dict = {}

    score = 0
    count_img_dict = {"MA"+str(k):0 for k in range(1,7)}

    for name, seq in list(reference_dict.items()):
        payload_id_check = seq[27:178]
        payload_id_check_reference_dict[payload_id_check] = name

        if payload_id_check_consensus_dict.get(payload_id_check, False):
            img_name = payload_id_check_reference_dict[payload_id_check][:3]
            count_img_dict[img_name] += 1
            score += 1
        elif not "MA5" in name and not "MA6" in name:
            print(">"+name)
            print(seq)

    #print(count_img_dict)
    print("consensus score : ",len(payload_id_check_consensus_dict),";",str(score))
    print()
    print()


def sort_scores(sequence_size):
    """
    read the score files to print a graph friendly version
    """
    scores_directory = "test_consensus/general_testing/scores_new/"

    dict_scores_assembly_number = {}
    assembly_number_list = [1,3,6,10,30,60,100,300]#,600,1000]

    for assembly_number in assembly_number_list:
        dict_scores_assembly_number[assembly_number] = {}
        score_file = scores_directory + "score_result_" + str(sequence_size) + "_" + str(assembly_number) + ".txt"
        with open(score_file, 'r') as f:
            score_lines = f.readlines()
        for line in score_lines:
            read_number, consensus_score, consensus_time = line.replace("\n", "").split(" ")
            dict_scores_assembly_number[assembly_number][read_number] = [float(consensus_score[:-1]), consensus_time[:-1]]

    merged_scores_file = scores_directory + "merged_scores_"+str(sequence_size)+".txt"
    with open(merged_scores_file, 'w') as f:
        f.write("assembly;"+";".join([str(n) for n in assembly_number_list])+"\n")
        for read_number in [str(10*k) for k in range(1, 11)]:
            line = read_number+";"
            for assembly_number in assembly_number_list:
                if read_number in dict_scores_assembly_number[assembly_number]:
                    score = round(dict_scores_assembly_number[assembly_number][read_number][0]/100,3)
                    line += str(score)+";"
                else:
                    line += "1;"
            f.write(line+"\n")

    merged_times_file = scores_directory + "merged_times_"+str(sequence_size)+".txt"
    with open(merged_times_file, 'w') as f:
        f.write("assembly;"+";".join([str(n) for n in assembly_number_list])+"\n")
        for read_number in [str(10*k) for k in range(1, 11)]:
            line = read_number+";"
            for assembly_number in assembly_number_list:
                if read_number in dict_scores_assembly_number[assembly_number]:
                    line += dict_scores_assembly_number[assembly_number][read_number][1]+";"
                else:
                    line += ";"
            f.write(line+"\n")


def consensus_testing():

    dir_path = "test_consensus/tests_2024/"
    
    reference_dir = dir_path+"references/"
    temp_files_dir = dir_path+"reads_files/"
    consensus_dir = dir_path+"consensus_files/"


    # file to store the simulated reads

    files_size_list = [3000, 5000] # length of sequences files
    files_number_list = [5000, 20000, 100000] # # number of different files

    coverage_list = [30,25,20,15] # number of reads per unique reference molecule

    kmer_size = 20
    min_occ = 2

    #for assembly_number in assembly_number_list:
    #    print("assembly number", assembly_number, "...")
    for file_size in files_size_list:
        print("file size :", str(file_size))
        perfect_score_streak = 0

        for file_number in files_number_list:
            print("file number :", str(file_number))
            test_name = str(file_number)+"_x_"+str(file_size)
            for coverage in coverage_list:
                consensus_time = 0

                reads_file = temp_files_dir+"merged_reads_"+test_name+".fastq"
                # create file with merged reads from different reference sequences
                reference_path = reference_dir+"ref_size_"+str(file_size)+".fasta"
                generate_random_reads(reference_path, file_number, coverage, reads_file)

                consensus_file = consensus_dir+"consensus_"+test_name+".fasta"

                start_time = time.time()
                # get the consensus file from this read
                rcc.kmer_consensus(reads_file, consensus_file, start_primer, stop_primer, kmer_size, min_occ, file_size)
                consensus_time += time.time()-start_time

                # get the accuracy of the consensus file
                score_consensus, false_positive = verify_consensus(consensus_file, reference_path, file_number)

                # save the resulting score
                with open(dir_path+"scores_"+str(file_size)+"_k"+str(kmer_size)+"_o"+str(min_occ)+"_c"+str(coverage)+".txt", 'a') as output:
                    output.write(str(file_number)+" "+ str(round(100*score_consensus/file_number, 2)) + "% "+str(false_positive)+" "+str(round(consensus_time, 3))+"s\n")
                print("score : "+str(round(100*score_consensus/file_number, 2)) + "% "+str(round(consensus_time, 3))+"s\n")

                # delete the read file
                os.remove(reads_file)


if __name__ == "__main__":


    print("testing consensus...")
    #for i in [1000*k for k in range(1,21)]:
    #    min_occ_testing(i)
    #for i in range(1):
    #    consensus_testing(3000)

    consensus_testing()
    #consensus_testing(50000)
    #s = verify_check_sum("test_consensus/consensus_test.fasta")
    #print(s)       
    print("\tcompleted !")

