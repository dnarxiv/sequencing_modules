#!/bin/bash

score_threshold=$1

if test -z "$score_threshold"
then
	score_threshold=7 #default score threshold
fi

echo "______________ analysis fragments from all barecodes, score threshold of $score_threshold ______________"

data_name=data_chloe_2

barcodes=("01" "02" "03" "01_fail" "02_fail" "03_fail")

fragments=("gBlock01")

mkdir 2_analysis/$data_name

for i in $(seq 0 5); do
	result_file=1_results/$data_name/extraction_bc${barcodes[i]}.txt
	echo "$result_file"
	
	fragments_file=0_data/fragments/${fragments[0]}_fw_rv.fasta #TODO frag[i]
	analysis_file=2_analysis/$data_name/bc${barcodes[i]}.txt #_mol${fragments[i]}.txt
	#analysis_file=results_analysis/$data_name/splited_${barcodes[i]}_analysis2
	#case_file=results_cases/$data_name/AJ_cases/${methods[i]}_cases_s7.txt 
	#rm $analysis_file.fastq
	python3 fragments_analysis.py -i "$result_file" -f "$fragments_file" -o "$analysis_file" -s $score_threshold
	#python3 cases_analysis.py -i $result_file -f $fragments_file -c "1" -o $case_file -s $score_threshold

done

#-------------- Exit --------------#
echo "___Fin du processus !___"

exit 0

# scp -r oboulle@dnarxiv.irisa.fr:~/Documents/result_analysis/fragment_assembly_analysis/results/demultiplexed_v2 ~/Documents/result_analysis/fragment_assembly_analysis/results
