#ifndef B84D3238_413F_430F_94CC_E2EF840C19EB
#define B84D3238_413F_430F_94CC_E2EF840C19EB

#include <algorithm>
#include <array>
#include <cstddef>
#include <cstdint>
#include <vector>

typedef int score_t;

constexpr score_t SW_GAP_SCORE = -7;
constexpr score_t SW_MISMATCH_SCORE = -5;
constexpr score_t SW_MATCH_SCORE = 10;

constexpr float SW_GOOD_SCORE_FACTOR = 0.95;
constexpr float SW_GOOD_ENOUGH_SCORE_FACTOR = 0.75;

constexpr size_t MAX_PRIMER_SIZE = 8;

class SmithWatermanWorker {
   public:
	struct AlignmentResult {
		score_t score;
		size_t pos;
	};

	SmithWatermanWorker(const size_t primer_size_);

	/// @return The **end** position of the maximum score alignment
	AlignmentResult align(const char* seq, const uint32_t seq_size, const char* primer);

	/// @return The **start** position of the maximum score alignment
	AlignmentResult align_reverse(const char* seq, const uint32_t seq_size, const char* primer);

	score_t get_max_possible_score() const { return primer_size * SW_MATCH_SCORE; }

   private:
	const size_t primer_size;
	score_t line0[MAX_PRIMER_SIZE + 1];
	score_t line1[MAX_PRIMER_SIZE + 1];

	static constexpr score_t sw_similarity(char a, char b) { return a == b ? SW_MATCH_SCORE : SW_MISMATCH_SCORE; }
};

#endif /* B84D3238_413F_430F_94CC_E2EF840C19EB */
