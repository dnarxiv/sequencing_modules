#include <iostream>
#include <fstream>
#include <filesystem>
#include <string>
#include <vector>
#include <algorithm>
#include <array>
#include <chrono>
#include <omp.h> // openMP for parralelism


#include "SmithWaterman.h" // include the header file for the Smith-Waterman algorithm



/*
threading handled by openMP
read all fastq, store in tab [read_id, read_seq, ""]
threads shares parts of the tab : get sequences, assign cluster id, fill the 3rd colomn with id
finaly loop over completed tab as many time as possible clusters
get all reads for cluster i, write it on a file

last part can be time consumming

*/

std::string reverse_complement(const std::string& sequence) {
    // get the reverse complement of a sequence
    std::string rc_sequence = sequence;
    std::transform(rc_sequence.begin(), rc_sequence.end(), rc_sequence.begin(), [](char c) {
        switch (c) {
            case 'A': return 'T';
            case 'C': return 'G';
            case 'G': return 'C';
            case 'T': return 'A';
            default: return c;
        }
    });
    std::reverse(rc_sequence.begin(), rc_sequence.end());
    return rc_sequence;
}


std::vector<std::string> read_fastq(const std::string& filename) {
    // function to read FASTQ file and return a vector of sequences
    std::ifstream file(filename);
    std::vector<std::string> sequences;
    std::string line;

    if (!file.is_open()) {
        std::cerr << "Error opening file: " << filename << std::endl;
        return sequences;
    }

    // Skip the header line
    while (std::getline(file, line)) {

        // Read the sequence line
        std::getline(file, line);
        sequences.push_back(line);

        // Skip the quality score lines
        std::getline(file, line);
        std::getline(file, line);
    }

    file.close();

    return sequences;
}


std::string get_cluster_id(const std::string& read, const std::string& seq_to_find, int split_level) {
    /*
    get the <split_level> bases following the primer in the read, also try to find the primer in the reverse complement sequence
    */

    double score_fw, score_rc; // alignment score with the forward and reverse complement sequence
    int x_fw, y_fw, x_rc, y_rc; // positions start and stop of the alignments found, x_fw and x_rc not used

    // call local alignment algorithm on the forward sequence
    SmithWaterman(read, seq_to_find, score_fw, x_fw, y_fw);

    // don't test for the reverse complement if near perfect alignment already found (maximum possible score = 10)
    if (score_fw >= 9.5 && read.size() > y_fw + split_level) {
        return read.substr(y_fw, split_level); // extract the cluster id
    }

    std::string read_rc = reverse_complement(read); // get reverse complement of the read sequence

    // test local alignement with the reverse complement sequence
    SmithWaterman(read_rc, seq_to_find, score_rc, x_rc, y_rc);

    if (score_fw >= score_rc) { // primer found in forward sequence
        if (score_fw >= 7.5 && read.size() > y_fw + split_level) {
            return read.substr(y_fw, split_level);
        }
    } else { // primer found in reverse complement sequence
        if (score_rc >= 7.5 && read_rc.size() > y_rc + split_level) {
            return read_rc.substr(y_rc, split_level);
        }
    }

    // score too weak, or read too small = ignore the read
    return "None";
}


void clustering(const std::string& reads_path, const std::string& start_primer, int split_level, const std::string& output_dir) {
    /* split_lvel = nbr of bases to use for the cluster id
    */

    //delete all fasta files in the clusters dir
    std::filesystem::path dir_path(output_dir);
    try {
        for (const auto& entry : std::filesystem::directory_iterator(dir_path)) {
            if (entry.path().extension() == ".fasta") {
                std::filesystem::remove(entry.path());
            }
        }
    } catch (const std::filesystem::filesystem_error& ex) {
        std::cerr << "Error deleting files: " << ex.what() << '\n';
    }

    int seq_to_find_size = std::min(static_cast<int>(start_primer.size()), 8);
    std::string seq_to_find = start_primer.substr(start_primer.size() - seq_to_find_size); // search for a smaller sequence than the full primer if its len is > 10

    std::ifstream input_read_file(reads_path); // read the fastq

    std::string read_name;
    std::string sequence;
    std::string line;

    // browse the reads file and skip the header line
    while (std::getline(input_read_file, read_name)) {

        // read the sequence line
        std::getline(input_read_file, sequence);

        // Skip the quality score lines
        std::getline(input_read_file, line);
        std::getline(input_read_file, line);

        // find the position for the primer and get the bases following it
        std::string cluster_id = get_cluster_id(sequence, seq_to_find, split_level);

        if (cluster_id == "None") {
            continue; // ignore the read
        }
        // write the sequence and its id to a cluster file corresponding to the bases found after the primer
        std::ofstream output_file(output_dir + "/" + cluster_id + ".fasta", std::ios::app); // ios app is to append in the file
        if (!output_file.is_open()) {
            std::cerr << "Error opening output file\n";
            return;
        }
        output_file << ">" << read_name << "\n" << sequence << "\n";
    }

    input_read_file.close();
}


 void fill_table_with_fastq_file(std::string** seq_table, const std::string& fastq_path) {
    // function to read FASTQ file, return length of filled tab

    std::string line;
    std::string read_name;
    std::string sequence;

    std::ifstream file(fastq_path);

    if (!file.is_open()) {
        std::cerr << "Error opening file: " << fastq_path << std::endl;
        return;
    }

    int num_reads = 0;
    // read the sequence name
    while (std::getline(file, read_name)) {
        // read the sequence
        std::getline(file, sequence);
        // fill the tab line
        std::array<std::string, 3> row = {read_name, sequence, ""};
        /*for (int i = 0; i < 3; i++) {
            seq_table[num_reads][i] = row[i];
        }*/
        //seq_table[num_reads] = row;
        std::copy(row.begin(), row.end(), seq_table[num_reads]);
        num_reads++;

        // skip the quality score lines
        std::getline(file, line);
        std::getline(file, line);

    }

    file.close();
}

void enumerate_clusters(std::vector<std::string>& cluster_id_list, const std::string& sequence, int split_level, const std::string& bases, int index) {
    // fill list of all possible clusters id
    if (index == split_level) {
        cluster_id_list.push_back(sequence);
        return;
    }

    for (char base : bases) {
        std::string new_sequence = sequence;
        new_sequence[index] = base;
        enumerate_clusters(cluster_id_list, new_sequence, split_level, bases, index + 1);
    }
}


void manage_threads(std::string** seq_table, int table_size, const std::string& start_primer, const std::string& output_dir_path) {

    // start a timer
    auto start = std::chrono::high_resolution_clock::now();

    int n_threads = 2;
    int split_level = 3;

    int seq_to_find_size = std::min(static_cast<int>(start_primer.size()), 8);
    std::string seq_to_find = start_primer.substr(start_primer.size() - seq_to_find_size); // search for a smaller sequence than the full primer if its len is > 10


    int lines_per_thread = (table_size + n_threads - 1) / n_threads;

    #pragma omp parallel for num_threads(n_threads) // openMp : define a fixed number of threads to handle the following loop
    for (int i_thread = 0; i_thread < n_threads; i_thread++) {

        printf("openMP : thread %d \n", omp_get_thread_num());

        int start_index = i_thread * lines_per_thread;
        int stop_index = (i_thread+1) * lines_per_thread;

        for (int index = start_index; index < stop_index && index < table_size; index++) { // browse the table part allocated to this thread

            std::string cluster_id = get_cluster_id(seq_table[index][1], seq_to_find, split_level); // get the cluster id for this read
            seq_table[index][2] = cluster_id; // store in the table
        }
    }

    auto end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsed = end - start;
    std::cout << elapsed.count() << "s for thread" << std::endl;
    // --- save reads in corresponding clusters --- //

    std::string bases = "ACGT";

    std::vector<std::string> cluster_id_list;
    std::string sequence(split_level, ' ');
    // get list of all possible clusters id
    enumerate_clusters(cluster_id_list, sequence, split_level, bases, 0);

    for (const std::string& cluster_id : cluster_id_list) {
        //std::cout << cluster_id << std::endl;
        std::string cluster_content = ""; // what will be written in the cluster file

        for (int index = 0; index < table_size; index++) { // loop over the full table of reads
            if (seq_table[index][2] == cluster_id) { // if read is associated to the cluster
                cluster_content += ">" + seq_table[index][0] + "\n" + seq_table[index][1] + "\n"; // add to content to be written
            }
        }
        // write the full cluster reads to the associated file
        std::ofstream output_file(output_dir_path + "/" + cluster_id + ".fasta", std::ios::app); // ios app is to append in the file
        if (!output_file.is_open()) {
            std::cerr << "Error opening output file\n";
            return;
        }
        output_file << cluster_content;
    }
    auto end2 = std::chrono::high_resolution_clock::now();
    elapsed = end2 - end;
    std::cout << elapsed.count() << "s for writting" << std::endl;
}


int main(int argc, char* argv[]) {

    // check if the input and output file paths are provided as arguments
    if (argc != 3) {
        std::cerr << "Usage: " << argv[0] << " <input_fastq> <output_dir>" << std::endl;
        return 1;
    } // ./fast_clustering reads.fastq clusters_dir_path

    // get the input and output paths from the arguments
    std::string input_fastq = argv[1];
    std::string output_dir = argv[2]; // must be an existing dir

    //delete all fasta files in the clusters dir
    std::filesystem::path dir_path(output_dir);
    try {
        for (const auto& entry : std::filesystem::directory_iterator(dir_path)) {
            if (entry.path().extension() == ".fasta") {
                std::filesystem::remove(entry.path());
            }
        }
    } catch (const std::filesystem::filesystem_error& ex) {
        std::cerr << "Error deleting files: " << ex.what() << '\n';
    }
    
    // start a timer
    auto start = std::chrono::high_resolution_clock::now();

    std::string start_primer = "GTTCAGAGTTCTACAGTCCGACGATCC";

    // get line_number
    std::ifstream file(input_fastq);
    int line_count = 0;
    std::string line;
    while (std::getline(file, line)) {
        line_count++;
    }

    int read_count = line_count/4;

    // init read table // contain seq_name; sequence; cluster_id(empty)
    std::string** seq_table = new std::string*[read_count];
    for (int i = 0; i < read_count; i++) {
        seq_table[i] = new std::string[3];
    }

    fill_table_with_fastq_file(seq_table, input_fastq);

    manage_threads(seq_table, read_count, start_primer, output_dir);

    // end the timer and print the elapsed time
    auto end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsed = end - start;
    std::cout << elapsed.count() << "s for fast clustering" << std::endl;

     for (int i = 0; i < read_count; i++) {
        delete[] seq_table[i];
    }
    delete[] seq_table;

    return 0;
}

//g++ -o fast_clustering -fopenmp fast_clustering_t2.cpp SmithWaterman.cpp && ./fast_clustering reads.fastq cluster_dir
