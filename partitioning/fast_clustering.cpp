#include <algorithm>
#include <chrono>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "SmithWaterman.hpp"  // include the header file for the Smith-Waterman algorithm

std::string reverse_complement(const std::string& sequence) {
	// get the reverse complement of a sequence
	std::string rc_sequence = sequence;
	std::transform(rc_sequence.begin(), rc_sequence.end(), rc_sequence.begin(), [](char c) {
		switch (c) {
			case 'A':
				return 'T';
			case 'C':
				return 'G';
			case 'G':
				return 'C';
			case 'T':
				return 'A';
			default:
				return c;
		}
	});
	std::reverse(rc_sequence.begin(), rc_sequence.end());
	return rc_sequence;
}


std::string get_cluster_id(const std::string& read, const std::string& seq_to_find, int split_level,
						   SmithWatermanWorker& sw_worker, const score_t good_score_threshold,
						   const score_t good_enough_score_threshold) {
	/*
	get the <split_level> bases following the primer in the read, also try to find the primer in the reverse complement
	sequence
	*/

	// call local alignment algorithm on the forward sequence
	auto res_direct = sw_worker.align(read.data(), read.size(), seq_to_find.data());

	// don't test for the reverse complement if near perfect alignment already found (maximum possible score = 10)
	if (res_direct.score >= good_score_threshold && read.size() > res_direct.pos + split_level) {
		return read.substr(res_direct.pos, split_level);  // extract the cluster id
	}

	std::string read_rc = reverse_complement(read);	 // get reverse complement of the read sequence

	// test local alignement with the reverse complement sequence
	auto res_revcomp = sw_worker.align(read_rc.data(), read_rc.size(), seq_to_find.data());

	if (res_direct.score >= res_revcomp.score) {  // primer found in forward sequence
		if (res_direct.score >= good_enough_score_threshold && read.size() > res_direct.pos + split_level) {
			return read.substr(res_direct.pos, split_level);
		}
	} else {  // primer found in reverse complement sequence
		if (res_revcomp.score >= good_enough_score_threshold && read_rc.size() > res_revcomp.pos + split_level) {
			return read_rc.substr(res_revcomp.pos, split_level);
		}
	}

	// score too weak, or read too small = ignore the read
	return "None";
}

void clustering(const std::string& reads_path, const std::string& start_primer, int split_level,
				const std::string& output_dir) {
	/* split_lvel = nbr of bases to use for the cluster id
	 */

	// delete all fasta files in the clusters dir
	std::filesystem::path dir_path(output_dir);
	try {
		for (const auto& entry : std::filesystem::directory_iterator(dir_path)) {
			if (entry.path().extension() == ".fasta") {
				std::filesystem::remove(entry.path());
			}
		}
	} catch (const std::filesystem::filesystem_error& ex) {
		std::cerr << "Error deleting files: " << ex.what() << '\n';
	}

	int seq_to_find_size = std::min(start_primer.size(), MAX_PRIMER_SIZE);
	std::string seq_to_find =
		start_primer.substr(start_primer.size() -
							seq_to_find_size);	// search for a smaller sequence than the full primer if its len is > 10

	std::ifstream input_read_file(reads_path);	// read the fastq

	std::string read_name;
	std::string sequence;
	std::string line;

	SmithWatermanWorker sw_worker(seq_to_find.size());
	auto max_possible_score = sw_worker.get_max_possible_score();
	score_t good_score_threshold = max_possible_score * SW_GOOD_SCORE_FACTOR;
	score_t good_enough_score_threshold = max_possible_score * SW_GOOD_ENOUGH_SCORE_FACTOR;

	// browse the reads file and skip the header line
	while (std::getline(input_read_file, read_name)) {
		// read the sequence line
		std::getline(input_read_file, sequence);

		// Skip the quality score lines
		std::getline(input_read_file, line);
		std::getline(input_read_file, line);

		// find the position for the primer and get the bases following it
		std::string cluster_id = get_cluster_id(sequence, seq_to_find, split_level, sw_worker, good_score_threshold,
												good_enough_score_threshold);

		if (cluster_id == "None") {
			continue;  // ignore the read
		}
		// write the sequence and its id to a cluster file corresponding to the bases found after the primer
		std::ofstream output_file(output_dir + "/" + cluster_id + ".fasta",
								  std::ios::app);  // ios app is to append in the file
		if (!output_file.is_open()) {
			std::cerr << "Error opening output file\n";
			return;
		}
		output_file << ">" << read_name << "\n" << sequence << "\n";
	}

	input_read_file.close();
}

int main(int argc, char* argv[]) {
	// check if the input and output file paths are provided as arguments
	if (argc != 3) {
		std::cerr << "Usage: " << argv[0] << " <input_fastq> <output_dir>" << std::endl;
		return 1;
	}  // ./fast_clustering reads.fastq clusters_dir_path

	// get the input and output paths from the arguments
	std::string input_fastq = argv[1];
	std::string output_dir = argv[2];  // must be an existing dir

	// start a timer
	auto start = std::chrono::high_resolution_clock::now();

	std::string start_primer = "GTTCAGAGTTCTACAGTCCGACGATCC";

	clustering(input_fastq, start_primer, 3, output_dir);

	// end the timer and print the elapsed time
	auto end = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> elapsed = end - start;
	std::cout << elapsed.count() << "s for fast clustering" << std::endl;

	return 0;
}

// g++ -o fast_clustering fast_clustering.cpp SmithWaterman.cpp -lstdc++fs -std=c++17 -O3 -g -Wall -Wextra && ./fast_clustering reads.fastq cluster_dir
