#!/usr/bin/python3

import os
import sys
import inspect
import time
import random

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, os.path.dirname(os.path.dirname(currentdir))+"/synthesis_modules")

import dna_file_reader as dfr
import synthesis_simulation as ss



# read fastq
# get minimizers
# hash dict key: minimizer, value: list of reads
# sort to file: line X : list of reads with common minimizer with read X


def minimizer_to_number(minimizer: str):
    """
    convert a minimizer to the associated number
    used to link to an index in the list of minimizer
    """
    base_to_num_dict = {"A":"00", "C":"01", "G": "10", "T":"11"}
    number_bin = ""
    for base in minimizer:
        number_bin += base_to_num_dict[base]
    number = int(number_bin, 2)

    return number


def get_minimizers(reads_path: str):
    """
    get a dict of minimizers from the read file
    """

    start = time.time()

    window_size = 10 # window to look for the minimizer
    minimizer_size = 6 # length of minimizer kmer

    minimizers_number = int(4**6) # 4 power lengh of minimizers
    minimizer_list = [[] for _ in range(minimizers_number)] # each minimizer possible is associated to an index in the list,
        # list at index i = list of reads that contains minimizer i

    reads_dict = dfr.read_fastq(reads_path)

    for read_id, read_seq in enumerate(reads_dict.values()):
        sequence_size = len(read_seq)
        read_seq_rv = dfr.reverse_complement(read_seq)
        last_minimizer_added = "Z"*minimizer_size

        for i in range(sequence_size-window_size+1):
            sub_fw = read_seq[i:i+window_size]
            sub_rv = read_seq_rv[sequence_size-window_size-i:sequence_size-i]

            minimizer = "Z"*minimizer_size # default to erase with first real minimizer found

            for j in range(window_size-minimizer_size+1): # this is far from an optimal search
                sub_minimizer = sub_fw[j:j+minimizer_size]
                if sub_minimizer < minimizer:
                    minimizer = sub_minimizer

                sub_minimizer = sub_rv[j:j+minimizer_size]
                if sub_minimizer < minimizer:
                    minimizer = sub_minimizer

            if minimizer != last_minimizer_added: # avoid duplicates
                minimizer_index = minimizer_to_number(minimizer)
                minimizer_list[minimizer_index].append(read_id)
                last_minimizer_added = minimizer

    print(time.time()-start,"s for minimizer list")

    return minimizer_list, len(reads_dict)


def minlist_to_graph(reads_number, minimizer_list, graph_file_path) -> None:
    """
    convert the list of minimizer to a graph for metis input
    line i : list of reads_id that share a minimizer with read of id=i
    """

    graph_lines = [[] for _ in range(reads_number)] # each line corresponds to a reads, content is other reads that shares at least 1 minimizer
    start = time.time()

    #print(minimizer_list)
    for neighbours_list in minimizer_list:
        for i, read_id in enumerate(neighbours_list):
            # add the list of neighbour for each id
            # node ids starts at 1 for metis
            read_id_neighbours = [str(k+1) for k in neighbours_list[:i] + neighbours_list[i+1:]]
            graph_lines[read_id] += read_id_neighbours

    edge_number = sum(len(l) for l in graph_lines) //2

    with open(graph_file_path, "w") as output_graph_file:
        output_graph_file.write(str(reads_number)+" "+str(edge_number))
        for neighbours_list_by_id in graph_lines:
            output_graph_file.write("\n" +" ".join(neighbours_list_by_id))

    print(time.time() - start, "min_list of graph")



def metis_output_to_soluce(metis_soluce_path, output_path):
    """
    convert the output of metis to a soluce file for validity check
    """

    soluce_dict = {}

    with open(metis_soluce_path, "r") as metis_soluce:
        line = metis_soluce.readline().replace("\n", "")
        line_number = 0
        while line != "":
            soluce_dict[line] = soluce_dict.get(line, []) + [line_number]
            line = metis_soluce.readline().replace("\n", "")
            line_number += 1

    with open(output_path, "w") as output_soluce:
        for i in range(len(soluce_dict)):
            line = ", ".join(["r"+str(k) for k in soluce_dict[str(i)]])
            output_soluce.write(line+"\n")


    #print(soluce_dict)


def kp_iter_bic_output_to_soluce(cluster_dir_path, output_path):

    soluce_dict = {}
    cluster_index = 0
    num_clusters = len(os.listdir(cluster_dir_path))
    # read the clusters in their creation order
    for i in range(num_clusters):
        file_path = os.path.join(cluster_dir_path, "cluster_"+str(i)+".txt")
        with open(file_path, 'r') as input_cluster:
            cluster_lines = input_cluster.readlines()
        cluster_reads = ["r"+line.replace("\n","") for line in cluster_lines]
        soluce_dict[cluster_index] = cluster_reads
        cluster_index += 1

    with open(output_path, "w") as output_soluce:
        for i in range(len(soluce_dict)):
            line = ", ".join(soluce_dict[i])
            output_soluce.write(line+"\n")


def eval_soluce(real_soluce_path: str, soluce_path: str, result_output: str):
    """
    compare the proposed results to the correct solution
    """
    soluce_by_read_dict = {}

    with open(real_soluce_path, 'r') as input_real_soluce:
        line = input_real_soluce.readline()
        while line != "":
            cluster_name = int(line.split(":")[0])
            cluster_columns = line.split(":")[1].replace("\n","").split(",")
            for read_name in cluster_columns:
                soluce_by_read_dict[read_name] = cluster_name
            line = input_real_soluce.readline()

    result_dict = {}

    with open(soluce_path, 'r') as input_soluce:
        line = input_soluce.readline()
        cluster_num = 0
        while line != "":
            reads_list = line.replace("\n","").replace(" ","").split(",")
            result_dict[cluster_num] = reads_list
            cluster_num += 1
            line = input_soluce.readline()

    result_lines = []
    # replace each read number by the id of referrence cluster, and factorise
    for cluster_num, reads_list in result_dict.items():
        referrenced_dict = {}

        for read in reads_list:
            referrence = soluce_by_read_dict[read]
            referrenced_dict[referrence] = referrenced_dict.get(referrence, 0) + 1

        referrenced_result_line = str(sum([v for _,v in referrenced_dict.items()]))+" reads : "

        various_seq_sum = 0 # sum of sequences from small clusters
        # display by decreasing number of occurrences
        for ref in sorted(referrenced_dict, key=referrenced_dict.get, reverse=True):
            if referrenced_dict[ref] > 5:
                referrenced_result_line += "c"+str(ref)+"_x"+str(referrenced_dict[ref])+" "
            else:
                various_seq_sum += referrenced_dict[ref]
        if various_seq_sum > 0:
            referrenced_result_line += "(+"+str(various_seq_sum)+")"


        result_lines.append(referrenced_result_line)

    with open(result_output, 'w') as output_file:
        for line in result_lines:
            output_file.write(line+"\n")


def generate_dict_of_minimizers(min_size: int):
    """
    create a dict to associate a unique index to all minimizers of a defined size
    """
    min_dict = {}

    # get all sequences of size m
    def rec_get_sequences(m, sequence=""):
        if m == 0:
            return [sequence]
        else:
            sequences = []
            for base in ['A', 'C', 'G', 'T']:
                sequences += rec_get_sequences(m-1, sequence + base)
            return sequences

    all_possible_sequences = rec_get_sequences(min_size)

    minimizer_index = 0
    for seq in all_possible_sequences:
        # get canonical sequence
        canon_seq = min(seq, dfr.reverse_complement(seq))
        if canon_seq not in min_dict:
            # add to the dict if missing, with an unique index
            min_dict[canon_seq] = minimizer_index
            minimizer_index += 1

    result_output = "minimizers_"+str(min_size)+".txt"
    with open(result_output, 'w') as output_file:
        for minimizer, index in min_dict.items():
            output_file.write(minimizer+" "+str(index)+"\n")


def graph_generation(reads_dir) -> None:
    """
    generate a matrix of simulated reads
    column corresponds to a read
    line to a minimizer
    cell x y = 1 if the read y contains the minimizer x, else 0
    """

    reads_path = reads_dir + "/shuffled_reads.fastq"
    graph_file_path = reads_dir + "/reads.graph"

    minimizer_list, reads_number = get_minimizers(reads_path)
    minlist_to_graph(reads_number, minimizer_list, graph_file_path)



if __name__ == "__main__":


    print("generate graph...")

    dir_path = "matrix_tests/matrix_10k_2"

    graph_generation(dir_path)
    # cmd : gpmetis matrix_tests/matrix_10k_2/reads_graph.graph 226 -ufactor 300

    #metis_output_to_soluce(dir_path+"reads.graph.part.30_10p", dir_path+"/metis_soluce_10p.txt")

    print("\tcompleted !")

