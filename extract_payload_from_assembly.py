#!/usr/bin/python3

import os
import sys
import inspect
import argparse


currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, os.path.dirname(currentdir)+"/synthesis_modules")
import dna_file_reader as dfr
sys.path.insert(0, os.path.dirname(currentdir)+"/source_encoding")
import file_to_dna as ftd # get the biological constants for block size, etc...



def removes_non_payload_stuff(consensus_sequence: str) -> str:
    """"
    remove the non payload parts added for the block assembly
    
    n blocks of size X before assembly : 2 unknown variables
    
    assembly lose size because of buffers removal, bsaI removal, half of overhangs removal
    
    total assembly size after consensus = n*X - n*2*buffer - n*2*bsaI - (n+1)*overhang
    -> (total + ovh)/n + 2*buffer + 2*bsaI + ovh = X
    n can only be from 1 to 10
    X is integer between min_total_size and max_total_size, and the max possible with a minimal number of blocks
    """

    assembly_size = len(consensus_sequence)
    
    block_number = 0
    
    for n in range(1, ftd.n_block_max+1): # try the possibility with all possible number of blocks
        
        # deduce the block size for this number of block with the assembly size
        block_size = (assembly_size + ftd.overhang_size)/n + 2*ftd.buffer_size + 2*ftd.bsaI_size + ftd.overhang_size
        
        # block size must be an int, with size in defined thresholds
        # since the design must maximize the block size with a minimal number of block
        # then the first block size that respects the constraints is obligatory the one chosen by the design
        if int(block_size) == block_size and block_size <= ftd.max_total_block_size and block_size >= ftd.min_total_block_size:
            #print("found block size",str(block_size),"for assembly of",str(assembly_size))
            block_number = n
            break
    
    # no possible valid couple (block number, block size) for this assembly : invalid consensus
    if block_number == 0:    
        #print("warning no possible corresponding assembly for the consensus")
        # TODO use another possible consensus for this file
        return ""
    
    # extract the payload from the assembly
    payload_sequence = ""
    
    if block_number == 1:
        # only removes the primers
        payload_sequence += consensus_sequence[ftd.primer_size:-ftd.primer_size]
    else:
        # removes also overhangs between the blocks
        
        # length of payload in first and last blocks of an assembly
        block_extremity_payload_size = int(block_size) - ftd.primer_size - 2*ftd.buffer_size - 2*ftd.bsaI_size - 2*ftd.overhang_size
        # length of payload in blocks inside the assembly
        block_intern_payload_size = int(block_size) - 2*ftd.buffer_size - 2*ftd.bsaI_size - 2*ftd.overhang_size

        payload_sequence += consensus_sequence[ftd.primer_size: ftd.primer_size + block_extremity_payload_size]
        sequence_index = ftd.primer_size + block_extremity_payload_size + ftd.overhang_size
        
        for i in range(1, block_number-1):
            payload_sequence += consensus_sequence[sequence_index: sequence_index + block_intern_payload_size]
            sequence_index = sequence_index + block_intern_payload_size + ftd.overhang_size
        
        payload_sequence += consensus_sequence[sequence_index: sequence_index + block_extremity_payload_size]
        
    return payload_sequence



def extract_payload_container(input_dir_path: str, output_dir_path: str) -> None:
    """
    remove overhangs and primers from the consensus sequence files and save the resulting payload
    apply to all sequences contained in the fasta files
    """
    
    for filename in os.listdir(input_dir_path):
        file_path = os.path.join(input_dir_path, filename)
        output_file = os.path.join(output_dir_path, filename)
        
        sequences_dict = dfr.read_fasta(file_path)
        payload_dict = {}
        for sequence_name, sequence in sequences_dict.items():
            payload = removes_non_payload_stuff(sequence)
            if payload != "": # ignore failed extractions
                payload_dict[sequence_name+"_payload"] = payload
            
        dfr.save_dict_to_fasta(payload_dict, output_file)



if __name__ == "__main__":
    
    parser = argparse.ArgumentParser(description='extract the payload from an ordered eBlocks assembly')
    parser.add_argument('-i', action='store', dest='input_dir_path', required=True,
                        help='path to the directory of consensus sequences files')
    parser.add_argument('-o', action='store', dest='output_dir_path', required=True,
                        help='directory to save the extracted sequences')

    
    arg = parser.parse_args()
    
    print("payload extraction...")

    #removes_non_payload_stuff(arg.input_dir_path, arg.output_dir_path)
    extract_payload_container(arg.input_dir_path, arg.output_dir_path)
                
    print("\tcompleted !")

